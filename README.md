# bspwm wallspace -- Workspace desktop switcher

Small script to change the wallpaper based on the workspace that you are currently swithing to.
<img src="wallspace.gif" width="600" height="338"/>
